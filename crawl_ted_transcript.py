from bs4 import BeautifulSoup
import urllib
import codecs
import os,glob
import numpy as np
import urllib.request
import os
from os import listdir
from os.path import isfile, join
import time
from datetime import datetime
from random import randrange
import re
import subprocess

# #Just save when vi_param and ja_param are not empty 
# def save_data_2(vi_text, ja_text, vifname, jafname):
#     global num_video_skip
#     if vi_text and ja_text:
#         #print(vifname)
#         #print(jafname)
#         redundant_str = ["TED. com translations", "TED.com translations"]
#         for item in redundant_str:
#             index_item = vi_text.find(item)
#             if index_item != -1: 
#                 vi_text = vi_text[:index_item].strip()
#             index_item = ja_text.find(item)
#             if index_item != -1:           
#                 ja_text = ja_text[:index_item].strip()

#         f = open(vifname, "w", encoding='utf-8')
#         f.write(vi_text + "\n")
#         f.close() 

#         f = open(jafname, "w", encoding='utf-8')
#         f.write(ja_text + "\n")
#         f.close()           
#     else:
#         print("cancel saving empty data")
#         #num_video_skip += 1

"""
get "/talks/" + talkname
return:
   + False if it is empty page
   + True if it has content
"""
def enlist_talk_names(path, dict_):
    # import pudb; pudb.set_trace()
    global src_lang_
    global tgt_lang_
    global fail_crawl_page
    status = False
    try:
        time.sleep(1.5)
        r = urllib.request.urlopen(path).read()
        soup = BeautifulSoup(r, "lxml")
        # print(soup)
        talks= soup.find_all("a",class_='ga-link')     
        for i in talks:
            href = i.attrs['href'].replace("?language=" + tgt_lang_, "")
            if href.find('/talks/')==0:
                if dict_.get(href)!=1:                               
                    dict_[href]=1
                status = True        
    except Exception as e:
        print(e)
        print("error page")  
        fail_crawl_page.append(path)
    return status


"""
Just save when vi_param and ja_param are not empty 
"""
def save_data_1(vi_text, ja_text, output_dir, talk_name):
    global num_video_skip     
    global src_lang_
    global tgt_lang_
    if vi_text and ja_text:        
        # redundant_str = ["TED. com translations", "TED.com translations"]
        # for item in redundant_str:
        #     index_item = vi_text.find(item)
        #     if index_item != -1: 
        #         vi_text = vi_text[:index_item].strip()
        #     index_item = ja_text.find(item)
        #     if index_item != -1:           
        #         ja_text = ja_text[:index_item].strip()                    
        if not vi_text or not ja_text:
            print("empty file: "+ talk_name)
        else:
            f = open(output_dir + talk_name + "." + src_lang_, "w", encoding='utf-8')
            f.write(vi_text + "\n")
            f.close() 

            f = open(output_dir + talk_name + "." + tgt_lang_, "w", encoding='utf-8')
            f.write(ja_text + "\n")
            f.close()           
    else:        
        print("cancel saving empty data")
        print("empty file: "+ talk_name)
        # num_video_skip += 1

"""
f_folder: contains files need to be listed
detect_str: {"nhat" | ".ja"} or {"viet" | ".vi"}
detectByExtension: {.vi | .ja} if true else {"nhat" | "viet"}

return:
   list of pure_name without extension of files which are sorted
"""
def get_files(f_folder):    
    files = [f[:-3] for f in listdir(f_folder) if isfile(join(f_folder, f))]
    return files

def convert_2_speech_per_line(ori_text): 
    #tmp_text = ori_text.replace('\t', '\n')   
    tmp_text = ori_text.replace('\n', ' ').replace('\t', '\n')
    while tmp_text.find("  ") != -1:
        tmp_text = tmp_text.replace('  ', ' ')
    while tmp_text.find("\n\n") != -1:
        tmp_text = tmp_text.replace('\n\n', '\n')                                           
    tmp_text = tmp_text.strip()    
    return tmp_text

"""
convert_text_2_lines and remove empty lines
"""
def convert_text_2_lines(text):
    # print(text)
    redundant_str = ["TED.com translations are made possible by volunteer translators. Learn more about the Open Translation Project."\
            , "TED.com translations are made possible by volunteer translators. Learn more about the Open Translation Project."\
            , "© TED Conferences, LLC. All rights reserved."]
    segments = []
    text_segments = text.split('\n')
    for line in text_segments:
        line = line.strip()
        if line and line not in redundant_str:
            segments.append(line)
    # print(segments)
    return segments

"""
strip all sentences in a text
remove redundant string
"""
def strip_text(text):
    redundant_str = ["TED.com translations are made possible by volunteer translators. Learn more about the Open Translation Project."\
            , "TED.com translations are made possible by volunteer translators. Learn more about the Open Translation Project."\
            , "© TED Conferences, LLC. All rights reserved."]

    clean_text = ""
    text_lines = text.split("\n")
    for line in text_lines:
        if line not in redundant_str:
            clean_text += line.strip() + "\n"
    return clean_text.strip()

def merge_all_params(list_param, spliting_param_by_empty_line=False):    
    final_text = ""
    spliting_param_char = '\n\n' if spliting_param_by_empty_line else '\n'
    for param in list_param: 
        final_text += param.strip() + spliting_param_char
    return final_text.strip()

"""
correct some cases like: Toi.Anh di choi. => Toi. Anh di choi.
"""
def correct_spliting_sen(text):
    global list_end_sign
    correct_text = ""
    for i in range(0, len(text)):
        if i < len(text) - 1\
           and text[i] in list_end_sign\
           and (text[i+1] not in list_end_sign and text[i+1] != ' '):
            correct_text += text[i] + ' '
        else:
            correct_text += text[i]
    return correct_text

"""
+ regex_end_sign thì bỏ đi cx được, chưa dùng.
+ list_end_sign thì gồm cả dấu kết thúc câu của cặp ngôn ngữ
+ map_vi2ja_end_sign thì là một dict map giữa tiếng

#user input
"""
##km-vi

regex_end_sign = "[\.\?\!;:…]"
list_end_sign = ['.','?','!',';','…', '。', '？', '！', ';','។']
map_vi2ja_end_sign = {'.':'។', '?':'？', '!':'！', ';':';', '…':'…'}

## ja-vi
# regex_end_sign = "[\.\?\!;:…]"
# list_end_sign = ['.','?','!',';','…', '。', '？', '！', ';']
# map_vi2ja_end_sign = {'.':'。', '?':'？', '!':'！', ';':';', '…':'…'}
"""
already check conditions to convert based on the input text:
 + The number of lines in two params vi and ja is equal.
 + Not empty in two params vi and ja.
check end_line based on vi_text to add the end_sign for ja_text
 + just when end_sign is in the end of the sentence
 + remove all empty lines if convert successfully (status==True) else keep the emtpy lines.
 + remove all unnecessary tokens in the head and tail of a sentence.

ja_param: ja_text
vi_param: vi_text
return an json object contains:
    final_ja_param: a CLEAN ja paragraph whose each line is a ja sentence with end_sign
    final_vi_param: a CLEAN vi paragraph whose each line is a vi sentence with end_sign
    + res['final_vi_param'] and res['final_ja_param'] can be empty if the two vi_param and ja_param are empty
"""
def convert_2_sentence_per_line(vi_param, ja_param):    
    global num_video_skip        
    final_ja_param = ""
    final_vi_param = ""
    status = False
    if ja_param and vi_param:
        ja_segments = convert_text_2_lines(ja_param) #remove empty line as well
        vi_segments = convert_text_2_lines(vi_param) #remove empty line as well
        if len(ja_segments) == len(vi_segments):
            # print(ja_segments)
            # print(vi_segments)
            vi_sentence = ""
            ja_sentence = ""
            len_segment = len(vi_segments)
            for i in range(0, len_segment):
                vi_seg = vi_segments[i].strip() #assure that each line is stripped
                ja_seg = ja_segments[i].strip() #assure that each line is stripped
                
                #check end_line based on vi_text to add end_sign for ja_text
                #just when end_sign is in the end of the sentence.
                if (vi_seg[-1] in list_end_sign) or (i == len_segment - 1):                    
                    end_sign = map_vi2ja_end_sign[vi_seg[-1]]\
                                    if vi_seg[-1] in list_end_sign and ja_seg[-1] not in list_end_sign\
                                    else ""
                    vi_sentence += correct_spliting_sen(vi_seg)
                    ja_sentence += ja_seg + end_sign  #add end_sign for ja_text                        
                    final_vi_param += vi_sentence.strip() + '\n'
                    final_ja_param += ja_sentence.strip() + '\n'
                    vi_sentence = ""    
                    ja_sentence = ""                               
                else:
                    vi_sentence += vi_seg + " "
                    ja_sentence += ja_seg + " "
             #final_vi_param = final_vi_param.strip()
             #output
            final_vi_param = final_vi_param.strip()
            final_ja_param = final_ja_param.strip()  
            status = True
        else: #cannot add end_sign for ja_text using vi_text
            #output
            final_vi_param = strip_text(vi_param)
            final_ja_param = strip_text(ja_param)
            num_video_skip += 1
    else: #files are empty
        num_video_skip += 1
    
    res = {"status": status,
            "final_vi_param": final_vi_param,
            "final_ja_param": final_ja_param}
    # print(res)
    return res

def get_links_and_langs(target_link, target_lang, all_link):
    global src_lang_
    global tgt_lang_

    for i in all_link:
        if i.get('href')!=None and i.attrs['href'].find('?language=')!=-1:
            if (src_lang_ == i.attrs['hreflang'] or tgt_lang_ == i.attrs['hreflang'])\
                and len(target_lang) < 2:
                target_link.append(i)
                target_lang.append(i.attrs['hreflang'])            

"""
vi_params a list of vi transcript segments
ja_params a list of ja transcript segments  
"""
def get_transcript(vi_params, ja_params, target_link):
    global src_lang_
    global tgt_lang_

    if len(target_link) != 2:
        return

    for i in target_link:
        if i.get('href')!=None and i.attrs['href'].find('?language=')!=-1:                
            lang=i.attrs['hreflang']
            path=i.attrs['href'] 
            time.sleep(1.0)           
            r1=urllib.request.urlopen(path).read()
            soup1=BeautifulSoup(r1, "lxml")             
            text_params= []   
            #each <p>             
            for i in soup1.findAll('p'):      
                text_in_timestamp = convert_2_speech_per_line(i.text).strip()      
                if text_in_timestamp:                            
                    text_params.append(text_in_timestamp)                
            if lang == tgt_lang_:
                ja_params += text_params
            else:
                vi_params += text_params
            print("get_transcript: " + path)  

"""
format_and_save by using the script in <p> not the whole script of subtitle in .ja and .vi
there are two cases:
+ success: The output data does not need to align by hand
+ fail: The output data needs to be aligned by hand

vi_params a list of vi transcript segments
ja_params a list of ja transcript segments  
"""
def format_in_timestamp(vi_params, ja_params,\
                output_dir_fail, output_dir_success, talk_name):
    fail_vi_params = []
    fail_ja_params = []
    success_vi_params = []
    success_ja_params = []
    for i in range(0, len(vi_params)):
        res = convert_2_sentence_per_line(vi_params[i], ja_params[i])        
        if res["status"]:
            success_vi_params.append(res["final_vi_param"])
            success_ja_params.append(res["final_ja_param"])
        else:
            fail_vi_params.append(res["final_vi_param"])
            fail_ja_params.append(res["final_ja_param"])        
    cat_success_vi_text = merge_all_params(success_vi_params)
    cat_success_ja_text = merge_all_params(success_ja_params)
    spliting_param_by_empty_line = True
    cat_fail_vi_text = merge_all_params(fail_vi_params, spliting_param_by_empty_line)
    cat_fail_ja_text = merge_all_params(fail_ja_params, spliting_param_by_empty_line) 
    if cat_success_vi_text and cat_success_ja_text:  #avoid misunderstanding when using checking in save_data_1 func  
        save_data_1(cat_success_vi_text, cat_success_ja_text\
                ,output_dir_success[_case["TH1"]], talk_name)
    if cat_fail_vi_text and cat_fail_ja_text: #avoid misunderstanding when using checking in save_data_1 func
        save_data_1(cat_fail_vi_text, cat_fail_ja_text\
                ,output_dir_fail, talk_name)

"""
format_and_save by using the whole script of subtitle in .ja and .vi
there are two cases:
+ success: The output data does not need to align by hand
+ fail: The output data needs to be aligned by hand

vi_params a list of vi transcript segments
ja_params a list of ja transcript segments  
"""
def format_in_whole_subtitle(vi_params, ja_params,\
                        output_dir_fail, output_dir_success, talk_name):    
    #   import pudb; pudb.set_trace()
    global _case
    # print("format_in_whole_subtitle: " + talk_name)
    spliting_param_by_empty_line = True
    vi_text = merge_all_params(vi_params, spliting_param_by_empty_line)
    ja_text = merge_all_params(ja_params, spliting_param_by_empty_line)
    res = convert_2_sentence_per_line(vi_text, ja_text)
    target_folder = output_dir_success[_case["TH2"]] if res["status"] else output_dir_fail
    save_data_1(res["final_vi_param"]\
                ,res["final_ja_param"]\
                , target_folder, talk_name)

""" 
There are many cases.
Currenty, they are:
+ TH1: format using script in <p> tag (== timestamp) => Success or Fail in each timestamp
+ TH2: format using script of the whole subtitles .vi and .ja. => Success or Fail in the whole transcript of subtitle

vi_params a list of vi transcript segments
ja_params a list of ja transcript segments  
"""
def format_subtitle_and_saving(vi_params, ja_params,\
                output_dir_fail, output_dir_success, talk_name):    

    if len(ja_params) == len(vi_params): #The number of <p> in sub file.vi and file.ja is equal
        format_in_timestamp(vi_params, ja_params,\
                        output_dir_fail, output_dir_success, talk_name)        
    else:
        format_in_whole_subtitle(vi_params, ja_params,\
                        output_dir_fail, output_dir_success, talk_name)
    
def extract_talk(path,talk_name,output_dir_success,output_dir_fail):    
    global num_video_skip   
    global prev_output_files
    global total_video
    if talk_name in prev_output_files:
        return 

    total_video += 1
    try:
        time.sleep(1.0)
        r=urllib.request.urlopen(path).read()    
        soup=BeautifulSoup(r, "lxml")              
        
        #GET links and langs
        target_link = []
        target_lang = []
        all_link = soup.findAll('link')
        get_links_and_langs(target_link, target_lang, all_link)     

        #CHECK valid videos
        if len(target_lang) != 2 or len(target_link) != 2:
            num_video_skip += 1
            return

        #GET DATA
        ja_params = []
        vi_params = []
        get_transcript(vi_params, ja_params, target_link)
                       
        format_subtitle_and_saving(vi_params, ja_params\
                ,output_dir_fail, output_dir_success, talk_name)
    except Exception as e:
        print(e)
        print("error video")
        fail_crawl_link.append(path)
        num_video_skip += 1

if __name__ == '__main__':   
    """
    TH1: extract by timespan
    TH2: extract by whole script
    """
    src_lang_ = "vi" #user input
    tgt_lang_ = "km" #user input

    _case = {"TH1": 0, "TH2": 1}
    NUM_REPEAT = 5#user input
    for i in range(0,NUM_REPEAT):
        #1. PROCESSING
        print("Time number {}".format(i + 1))        
        current_dir = os.path.dirname(os.path.realpath(__file__))
        output_dir_success = [current_dir + "/crawl_ted_talk_success_TH1/", current_dir + "/crawl_ted_talk_success_TH2/"] #user input
        output_dir_fail = current_dir + "/crawl_ted_talk_need_align/" #user input
        prev_output_files = []
        for item in output_dir_success:
            prev_output_files += get_files(item) #comment this line To recrawl all unique files in output_dir_success TH1 and TH2
        prev_output_files += get_files(output_dir_fail) #comment this line To recrawl all files in output_dir_fail (TH4) maybe dup with TH1
        # prev_output_files += get_files(current_dir + "/crawl_ted_talk_success_TH3/") #comment this line To recrawl all files in TH3 maybe dup with TH1
        prev_output_files = list(set(prev_output_files))
        min_page = 1 #user input
        max_page = 88 #user input  
        fail_crawl_link = []  
        fail_crawl_page = []    
        num_page_skip = 0    
        num_video_skip = 0 
        total_video = 0       
        all_talk_names={}
        for i in range(min_page,max_page+1):
           path="https://www.ted.com/talks?sort=newest&language=km&page=%d&sort=newest"%(i)#user input
           """           
           Xem link https://www.ted.com/talks để tìm ngôn ngữ lào hoặc khmer hoặc trung trong phần lọc của trang ted
           """
           print(path)
           status=enlist_talk_names(path,all_talk_names)    
        # print(all_talk_names)                 

        #FOR TEST A SPECIFIC LINKS
        # all_talk_names = ["/talks/frank_gehry_as_a_young_rebel"]
        for i in all_talk_names:
            extract_talk('https://www.ted.com'+i+'/transcript',i[7:], output_dir_success, output_dir_fail)        

        #2. LOG INFO
        total_page = max_page - min_page + 1        
        print("total_page: {}".format(total_page))
        print("num_page_crawl_fail: {}".format(num_page_skip))
        print("num_page_success: {}".format(total_page - num_page_skip))
        print("num_total_video: {}".format(total_video))
        print("num_video_crawl_fail: {}".format(len(fail_crawl_link)))        
        # print("num_video_skip: {}".format(num_video_skip))
        # print("num_video_success: {}".format(total_video - num_video_skip))

        #3. SAVING ERROR FILES
        f = open(current_dir + "/fail/fail_crawl_video.txt", 'w', encoding='utf-8')#user input
        for i in fail_crawl_link:
            f.write(i + "\n")
        f.close()

        f = open(current_dir + "/fail/fail_crawl_page.txt", 'w', encoding='utf-8')#user input
        for i in fail_crawl_page:
            f.write(i + "\n")
        f.close()
        time.sleep(10.0) #user input
        #path='/home/ajinkyak/Workspace/Image/Parse_talks/'
        #os.chdir(path)
        #pieces=[]
        #for file in glob.glob('*.csv'):
        #    print(file)
        #    frame=pd.read_csv(path+file,sep='\t',encoding='utf-8')
        #    pieces.append(frame)
        #df= pd.concat(pieces, ignore_index=True)
