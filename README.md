# TED_TALK crawling

This project is used to crawl data from TED talks, align automatically based on the first-lang (src_lang_) having end signs that means the tgt_lang_ (target lang) is not required to have end signs in its description. Specifically, in the result:
+ Th1, Th2: Data is already aligned and can be used directly
+ Need_align: Need to be re-align by human
+ Fail: Not success when crawling

Parameter need to be edited (search "#user input") in code:
+ list_end_sign: The end signs of src_lang_ (source language) and tgt_lang_ (target language)
+ map_vi2ja_end_sign: A dictionary mapping between end signs of src_lang_ to tgt_lang_.
+ path="https://www.ted.com/talks?sort=newest&language=lo&page=%d"%(i)
  View: https://www.ted.com/talks to find out the source path with appropriate language
+ src_lang_ = "vi" #user input
+ tgt_lang_ = "ja" #user input

Please create corresponding directories when needed (see logs in terminal or watch in code)
